# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
DemoApp::Application.config.secret_key_base = 'ed4ffcafce9541b5085ac9861b8ff5aff34866efdc19d8a018b8258dcdc6de10f0e0ed552c55026c572662f324ae87b143f5bf51d1fcda21b596294d07746830'
